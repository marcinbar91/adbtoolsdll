﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADBToolsDLL
{
    public class InPhone
    {
        public static void ReplaceLine(string file, string old, string newF)
        {
            CMD.Shell("busybox sed -i 's~" + old + "~" + newF + "~g' " + file);
        }
        public static string FileFind(string filename, string path = "/system/")
        {
            return CMD.Shell("busybox find " + path + " -name " + filename).Trim();
        }
        public static string FileRemove(string path)
        {
            CMD.Shell("busybox rm -rf " + path);
            return "File removed";
        }
        public static bool FileExist(string path)
        {
            string result = CMD.Shell(@"[ -f " + path + @" ] && echo true|| echo false");
            return Convert.ToBoolean(result);
        }
        public static bool DirExists(string path)
        {
            string result = CMD.Shell(@"[ -d " + path + @" ] && echo true|| echo false");
            return Convert.ToBoolean(result);
        }
        public static string DirCreate(string path, int chmod = 0)
        {
            CMD.Shell("busybox mkdir -p " + path);
            if (chmod != 0)
                Chmod.Set(path, chmod);
            return "Dir Created";
        }
        public static string DirRemove(string path)
        {
            CMD.Shell("busybox rm -rf " + path);
            return "Dir Removed";
        }

        public static string Cat2(string path)
        {
            return CMD.Shell("cat " + path);
        }

        public static List<string> Cat(string path)
        {
            char[] delimiters = new char[] { '\r', '\n' };
            return CMD.Shell("cat " + path).Split(delimiters, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        public static void AddLine(string file, string prop)
        {
            string temp = CMD.Shell("grep " + prop + " " + file).Trim();
            if (!string.IsNullOrEmpty(temp))
            {
                Log.AppendNewLine("Line " + prop + " exist");
            }
            else
            {
                CMD.Shell("echo - e " + prop + ">> " + file);
                Log.AppendNewLine("Set " + prop);
            }
        }
        public static void RemoveLine(string file, string prop)
        {
            CMD.Shell("busybox sed '/ " + prop + @"/d' " + file).Trim();
        }

        public static void WipeDalvikCache()
        {
            CMD.Shell("rm -rf /data/dalvik-cache/*");
            Log.AppendNewLine("Dalvik Cache cleared");
        }
        public static void StartActivity(string activity)
        {
            CMD.Shell(@"am start -n " + activity);
            Log.AppendNewLine("Activity " + activity + "started");
        }

        public class Chmod
        {
            public static string Get(string path)
            {
                return CMD.Shell("busybox stat " + path + @" | busybox  sed -n '/^Access: (/{s/Access: (\([0-9]\+\).*$/\1/;p}'");
            }

            public static string Set(string path, string mod)
            {
                return CMD.Shell("chmod " + mod + " " + path);
            }
            public static string Set(string path, int mod)
            {
                return CMD.Shell("chmod " + mod.ToString() + " " + path);
            }
        }

        //public static string Exist(string path)
        //{
        //    Log.AppendNewLine("[ -f \"" + path + "\" ] && echo " + path);
        //    return CMD.Shell("[ -f \""+path+ "\" ] && echo " + path).Trim();
        //}
    }
}
