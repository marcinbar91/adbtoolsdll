﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADBToolsDLL
{
    public class BuildProp
    {
        public static string Show()
        {
            return CMD.RUN(@".\files\adb shell su -c cat /system/build.prop");
        }

        public static void Set(string prop, string param)
        {
            CMD.Shell("mount -o remount,rw /system");
            string temp = Get(prop);
            if (!string.IsNullOrEmpty(temp))
            {
                if (temp.Split('=').Last() != param)
                    Modify(prop, param);
                else
                    Log.AppendNewLine("Prop " + prop + " = " + param + " exist");
            }
            else
            {
                CMD.Shell("echo - e " + prop + "=" + param + ">> /system/build.prop");
                Log.AppendNewLine("Set " + prop + " = " + param);
            }
        }
        public static string Get(string prop)
        {
            return CMD.Shell("grep " + prop + " /system/build.prop").Trim();
        }
        public static void Remove(string prop)
        {
            CMD.Shell("grep " + prop + " /system/build.prop > /system/temp && mv /system/temp /system/build.prop");
            Log.AppendNewLine("Removed " + prop);
        }
        private static void Modify(string prop, string param)
        {
            CMD.Shell("busybox sed -i -e 's/" + Get(prop) + "/" + prop + " = " + param + "/g' /system/build.prop");
            Log.AppendNewLine("Modified " + prop + " = " + param);
        }
    }
}
