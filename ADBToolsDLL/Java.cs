﻿using Microsoft.Win32;

namespace ADBToolsDLL
{
    public class Java
    {
        public static string GetVersion()
        {
            string value64 = string.Empty;
            RegistryKey localKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
            localKey = localKey.OpenSubKey(@"SOFTWARE\JavaSoft\Java Runtime Environment");
            if (localKey != null)
            {
                value64 = localKey.GetValue("CurrentVersion").ToString();
            }
            return value64;
        }
        public static bool isJava()
        {
            string value64 = GetVersion();
            if (string.IsNullOrWhiteSpace(value64))
                return false;
            else
                return true;

        }
    }
}
