﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADBToolsDLL
{
    public class Zip
    {
        public static string InsertToAPK(string apk, string pathToFilerOrFolder)
        {
            if (File.Exists(Tools.PCdirTemp + apk))
            {
                return CMD.InsertToAPK(Tools.PreparePathForCMD(Tools.PCdirTemp + apk) , Tools.PreparePathForCMD(Tools.curdir + @"\" + pathToFilerOrFolder));
            }
            return string.Empty;
        }

    }
}
