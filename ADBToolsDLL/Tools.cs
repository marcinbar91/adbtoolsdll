﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADBToolsDLL
{
    public class Tools
    {

        public static string curdir = Environment.CurrentDirectory;
        //public static string curdirforcmd = curdir2.Replace(" ", "\" \"");
        //public static string decompileddir = @".\temp\decompiled\";
        //public static string donedir = @".\temp\done\";
        //public static string pulleddir = @".\temp\pulled\";


        public static string PCdirTemp = curdir + @"\temp\";



        public static string GetFullPath(string ss)
        {
            List<string> s = GetFilesAndFolders(Tools.curdir, 10);
            foreach (string dir in s)
            {
                if (dir.Contains(ss))
                    return dir;
            }
            return string.Empty;
        }


        private static List<string> GetFilesAndFolders(string root, int depth)
        {
            var list = new List<string>();
            foreach (var directory in Directory.EnumerateDirectories(root))
            {
                list.Add(directory);
                if (depth > 0)
                {
                    list.AddRange(GetFilesAndFolders(directory, depth - 1));

                }
            }
            list.AddRange(Directory.EnumerateFiles(root));
            return list;
        }
        public static string PreparePathForCMD(string path)
        {
            path = path.Trim();
            string curdir = Environment.CurrentDirectory;

            if (path.Substring(0, 2) == @".\")
                return path.Replace(" ", "\" \"");
            else if (path.Trim().Substring(0, curdir.Length) == curdir)
            {
                path = path.Replace(curdir, @".\").Replace(@".\\", @".\").Replace(" ", "\" \"");
                return path.Replace(curdir, @".\").Replace(" ", "\" \"");
            }
            return path.Replace(" ", "\" \"");
        }
    }

    public static class Extension
    {
        //string FullPath;
        //string FileName;
        //string OnlyPath;
        //string curdir = Environment.CurrentDirectory;


        public static string FileNameWithAPKExtension(this String str)
        {
            if (Path.GetFileName(str).Split('.').Last() == "")
                return Path.GetFileName(str) + ".apk";
            else if (Path.GetFileName(str).Split('.').Last() == "apk")
                return Path.GetFileName(str);
            else
                return str;
        }
        public static string FileNameWithoutExtension(this String str)
        {
            return Path.GetFileNameWithoutExtension(str);
        }


        private static string FragmentPath2FullPath(string fragmentPath, int depth = 5)
        {
            List<string> s = GetFilesAndFolders(Environment.CurrentDirectory, depth);
            foreach (string dir in s)
            {
                if (dir.Contains(fragmentPath))
                    return dir;
            }
            return string.Empty;
        }


        private static List<string> GetFilesAndFolders(string root, int depth)
        {
            var list = new List<string>();
            foreach (var directory in Directory.EnumerateDirectories(root))
            {
                list.Add(directory);
                if (depth > 0)
                {
                    list.AddRange(GetFilesAndFolders(directory, depth - 1));

                }
            }
            list.AddRange(Directory.EnumerateFiles(root));
            return list;
        }
    }
}
