﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ADBToolsDLL
{
    public class InPC
    {
        public static bool ReplaceLine(string apkandrespath, string old, string newF)
        {
            string dir = Tools.GetFullPath(apkandrespath);
            List<string> list = new List<string>(File.ReadAllLines(dir));
            if (!LineExist(list, old))
                return false;
            List<string> l = new List<string>();

            for (int i = 0; i < list.Count; i++)
                l.Add(list[i].Replace(old, newF));

            File.WriteAllLines(dir, l);
            return true;
        }

        public static void InsertLineIndex(string apkandrespath, int index, string toaddline, int padleft = 0)
        {
            string dir = Tools.GetFullPath(apkandrespath);
            List<string> list = new List<string>(File.ReadAllLines(dir));
            if (!(list[index].Contains(toaddline)))
                list.Insert(index, toaddline.SetPadLeft(padleft));
            File.WriteAllLines(dir, list);
        }
        public static void InsertLineIndex(string apkandrespath, int index, List<string> toaddline, int padleft = 0)
        {
            string dir = Tools.GetFullPath(apkandrespath);
            List<string> list = new List<string>(File.ReadAllLines(dir));
            File.WriteAllLines(dir, list.InsertListIndex(toaddline, index, padleft));
        }

        public static bool InsertLineBeforeLine(string apkandrespath, string line, string toaddline, int padleft = 0)
        {
            string dir = Tools.GetFullPath(apkandrespath);
            List<string> list = new List<string>(File.ReadAllLines(dir));
            if (!LineExist(list, line))
                return false;
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Contains(line) && !list[i - 1].Contains(toaddline))
                    list.Insert(i, toaddline.SetPadLeft(padleft));
            }
            File.WriteAllLines(dir, list);
            return true;
        }

        public static bool InsertLineAfterLine(string apkandrespath, string line, string toaddline, int padleft = 0)
        {
            string dir = Tools.GetFullPath(apkandrespath);
            List<string> list = new List<string>(File.ReadAllLines(dir));
            if (!LineExist(list, line))
                return false;
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Contains(line) && !list[i + 1].Contains(toaddline))
                    list.Insert(i + 1, toaddline.SetPadLeft(padleft));
            }
            File.WriteAllLines(dir, list);
            return true;
        }

        public static bool DeleteLine(string apkandrespath, string line)
        {
            string dir = Tools.GetFullPath(apkandrespath);
            List<string> list = new List<string>(File.ReadAllLines(dir));
            if (!LineExist(list, line))
                return false;
            List<string> l = new List<string>();
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Contains(line))
                    list.RemoveAt(i);
            }
            File.WriteAllLines(dir, l);
            return true;
        }
        public static bool DeleteAllBetweenTwoLine(string apkandrespath, string line, string toline)
        {
            string dir = Tools.GetFullPath(apkandrespath);
            List<string> list = new List<string>(File.ReadAllLines(dir));
            if (!LineExist(list, line))
                return false;
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Contains(line))
                {
                    i++;
                    while (!list[i].Contains(toline))
                        list.RemoveAt(i);

                }
            }
            File.WriteAllLines(dir, list);
            return true;
        }

        public static bool DeleteLineAfterLine(string apkandrespath, string line)
        {
            string dir = Tools.GetFullPath(apkandrespath);
            List<string> list = new List<string>(File.ReadAllLines(dir));
            if (!LineExist(list, line))
                return false;
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Contains(line))
                {
                    list.RemoveAt(i + 1);
                }
            }
            File.WriteAllLines(dir, list);
            return true;
        }

        public static bool DeleteLineBeforeLine(string apkandrespath, string line)
        {
            string dir = Tools.GetFullPath(apkandrespath);
            List<string> list = new List<string>(File.ReadAllLines(dir));
            if (!LineExist(list, line))
                return false;
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Contains(line))
                    list.RemoveAt(i - 1);
            }
            File.WriteAllLines(dir, list);
            return true;
        }

        public static string GetLineAfterLine(string apkandrespath, string line)
        {
            string dir = Tools.GetFullPath(apkandrespath);
            List<string> list = new List<string>(File.ReadAllLines(dir));
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Contains(line))
                    return list[i+1];
            }
            return "";
        }
        public static string GetLineBeforeLine(string apkandrespath, string line)
        {
            string dir = Tools.GetFullPath(apkandrespath);
            List<string> list = new List<string>(File.ReadAllLines(dir));
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Contains(line))
                    return list[i - 1];
            }
            return "";
        }
        public static bool GetBoolsXMLValue(string apkandrespath, string line)
        {
            string dir = Tools.GetFullPath(apkandrespath);
            List<string> list = new List<string>(File.ReadAllLines(dir));
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Contains(line))
                    return Convert.ToBoolean(Regex.Replace(list[i], @"<(.|\n)*?>", string.Empty));
            }
            return false;
        }

        public static bool LineExist(List<string> list, string line)
        {
            foreach (string s in list)
            {
                if (s.Contains(line))
                    return true;
            }
            return false;
        }







        //public static int GetPad(string line)
        //{
        //    for (int i = 0; i < line.Length; i++)
        //    {
        //        if (!(line[i] == ' '))
        //        {
        //            return i;
        //        }
        //    }
        //    return 0;
        //}
    }

    public static class MyExtensions
    {
        public static List<string> InsertListIndex(this List<string> list, List<string> toaddlist, int index, int padleft = 0)
        {
            List<string> temp = new List<string>();

            for (int i = 0; i < list.Count; i++)
            {
                if (i == index)
                    for (int j = 0; j < toaddlist.Count; j++)
                        temp.Add(toaddlist[j].SetPadLeft(padleft));
                temp.Add(list[i]);
            }

            return temp;
        }
        public static string SetPadLeft(this string line, int padleft)
        {
            if (padleft == 0)
                return line;
            else
                return line.PadLeft(line.Length + padleft);
        }
        public static string SetPadRight(this string line, int padright)
        {
            if(padright == 0)
                return line;
            else
                return line.PadRight(line.Length + padright);
        }

        public static int GetPad(this string line)
        {
            for (int i = 0; i < line.Length; i++)
            {
                if (!(line[i] == ' '))
                    return i;
            }
            return 0;
        }
    }
}
