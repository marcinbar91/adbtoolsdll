﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADBToolsDLL
{
    public class Apktool
    {
        public static void Install(string apk)
        {
            if (ADB.Pull(apk))
            {
                Log.AppendNewLine(apk + " installing");
                string result = CMD.Install(Tools.PreparePathForCMD(Tools.PCdirTemp + apk));
                if (result.Contains("Framework installed to"))
                {
                    Log.Append(" - done");
                    //File.Delete(Tools.PCdirTemp + apk);
                }
                else
                    Log.AppendNewLine(result);
            }
            else
                Log.AppendNewLine(apk + "does not exist");

        }

        public static string CompileAPK(string apkname, bool insertManifest = false, bool sign = false)
        {
            Log.AppendNewLine(apkname + " compiling");

            string fullpath2dir = Tools.PCdirTemp + apkname.FileNameWithoutExtension();
            string fulloutputpath2apk = Tools.PCdirTemp + apkname;

            if (File.Exists(fulloutputpath2apk))
                File.Delete(fulloutputpath2apk);
            string result = CMD.CompileAPK(Tools.PreparePathForCMD(fullpath2dir) + " -o " + Tools.PreparePathForCMD(fulloutputpath2apk));
            if (!result.Contains("Exception in thread"))
            {
                CMD.InstertMetaInfo(apkname, Tools.PreparePathForCMD(fullpath2dir));
                if (insertManifest)
                    CMD.InstertAndroidManifest(apkname, Tools.PreparePathForCMD(fullpath2dir));
                if (sign)
                    result = CMD.SignAPK(Tools.PreparePathForCMD(fulloutputpath2apk));
                //   Directory.Delete(Tools.PCdirdecompile + apkname.FileNameWithoutExtension(), true);
                Log.Append(" - done");
            }
            else
                Log.AppendNewLine(result);

            return result;

            //List<string> list = new List<string>(result.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries));
            //if (list.Last() == "done")
            //    Log.Append(apkname + " compile done");
            //else
            //    Log.Append(result);
        }

        public static bool DecompileAPK(string apkname)
        {
            if (!Directory.Exists(Tools.PCdirTemp + apkname))
                ADB.Pull(apkname);

            string fullpath2apk = Tools.PCdirTemp + apkname;
            string fulloutputpath2dir = Tools.PCdirTemp + apkname.FileNameWithoutExtension();

            Log.AppendNewLine(apkname + " decompiling");
            string result = CMD.DecompileAPK(Tools.PreparePathForCMD(fullpath2apk), Tools.PreparePathForCMD(fulloutputpath2dir));
            if (result.Contains("Exception in thread"))
                Log.AppendNewLine(result);
            else
            {
                Log.Append(" - done");
                //File.Delete(Tools.PCdirTemp + apkname);
            }
            //List <string> list = new List<string>(result.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries));
            //if (list.Last() == "done")
            //    Log.Append(apkname + " decompile done");
            //else
            //    Log.Append(result);
            return true;
        }

    }
}
