﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADBToolsDLL
{
    public class ADB
    {

        public static string Mount(string mount)
        {
            string result = CMD.Shell("mount -o remount,rw " + mount);
            return result;
        }
        public static bool WaitForDevice(int timeoutSeconds = -1)
        {
            DateTime cur = DateTime.Now;
            do
            {
                Console.WriteLine("Wait");
                do
                {
                    string result = CMD.ADB("devices");
                    List<string> list = new List<string>(result.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries));
                    if (list.Last() != "List of devices attached")
                    {
                        Console.WriteLine(result);
                        return true;
                    }
                } while (DateTime.Now.Subtract(cur).Seconds < timeoutSeconds);
            } while (timeoutSeconds == -1);

            return false;
        }
        public static bool Pull(string apk)
        {
            if (File.Exists(Tools.PCdirTemp + apk))
                return true;
            string path = InPhone.FileFind(apk);

            if (path == "")
            {
                Log.AppendNewLine(apk + " does not exist");
                //stop
                return false;
            }
            else
            {
                Log.AppendNewLine(apk + " pulling");
                CMD.ADB("pull " + path + " " + Tools.PreparePathForCMD(Tools.PCdirTemp + apk));
                Log.Append(" - done");
                return true;
            }

        }
        public static bool Push(string fullfilename, string pathinphone, int chmod = 0, string path2sdcard = @"/sdcard/")
        {
            string donepath = Tools.PCdirTemp + fullfilename;
            Log.AppendNewLine(fullfilename + " pushing and moving to " + pathinphone);
            string result = CMD.ADB("push " + Tools.PreparePathForCMD(donepath) + " " + path2sdcard);
            if (result.Contains("failed to copy") || result.Contains("cannot stat"))
            {
                Log.AppendNewLine(result.Trim().Split(':').Last());
                //stop
                return false;
            }
            else
            {
                InPhone.DirCreate(pathinphone, 644);
                result = CMD.Shell(@"cp /storage/emulated/0/" + fullfilename + " " + pathinphone);
                if (result.Contains("failed on") || result.Contains("No such") || result.Contains("Read-only"))
                {
                    Log.AppendNewLine(result);
                    return false;
                }
                if (chmod != 0)
                    InPhone.Chmod.Set(pathinphone + @"/" + fullfilename, chmod);
                //CMD.Shell("rm /storage/emulated/0/" + apk);
                Log.Append(" - done");
                return true;
            }
        }
        public static void Reboot()
        {
            CMD.Shell("reboot");
            Log.AppendNewLine("Rebooted");
        }

    }
}
