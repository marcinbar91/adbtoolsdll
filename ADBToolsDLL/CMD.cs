﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

using System.Text;
using System.Threading.Tasks;

namespace ADBToolsDLL
{
    public class CMD
    {
        public static string RUN(string command)
        {
            try
            {
                ProcessStartInfo procStartInfo =
                    new ProcessStartInfo("cmd", "/c " + command);

                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.UseShellExecute = false;
                procStartInfo.CreateNoWindow = true;
                Process proc = new Process();
                proc.StartInfo = procStartInfo;
                proc.Start();
                string result = proc.StandardOutput.ReadToEnd().Trim();
                proc.WaitForExit();
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string Shell(string command)
        {
            string dir = Environment.CurrentDirectory + @"\files\";
            if (!File.Exists(dir + "adb.exe"))
            {
                Directory.CreateDirectory(dir);
                File.WriteAllBytes(dir + "adb.exe", Resource1.adb);
            }
            if (!File.Exists(dir + "AdbWinApi.dll"))
            {
                Directory.CreateDirectory(dir);
                File.WriteAllBytes(dir + "AdbWinApi.dll", Resource1.AdbWinApi);
            }
            if (!File.Exists(dir + "AdbWinUsbApi.dll"))
            {
                Directory.CreateDirectory(dir);
                File.WriteAllBytes(dir + "AdbWinUsbApi.dll", Resource1.AdbWinUsbApi);
            }
            Console.WriteLine(@".\files\adb shell su -c """ + command + @"""");
            return RUN(@".\files\adb shell su -c """ + command+@"""");
        }
        public static string ADB(string command)
        {
            string dir = Environment.CurrentDirectory + @"\files\";
            if (!File.Exists(dir + "adb.exe"))
            {
                Directory.CreateDirectory(dir);
                File.WriteAllBytes(dir + "adb.exe", Resource1.adb);
            }
            if (!File.Exists(dir + "AdbWinApi.dll"))
            {
                Directory.CreateDirectory(dir);
                File.WriteAllBytes(dir + "AdbWinApi.dll", Resource1.AdbWinApi);
            }
            if (!File.Exists(dir + "AdbWinUsbApi.dll"))
            {
                Directory.CreateDirectory(dir);
                File.WriteAllBytes(dir + "AdbWinUsbApi.dll", Resource1.AdbWinUsbApi);
            }
            return RUN(@".\files\adb " + command);

        }


        internal static string InsertToAPK(string path, string pathToFileOrFolder)
        {
            string dir = Environment.CurrentDirectory + @"\files\";
            if (!File.Exists(dir + "7za.exe"))
            {
                Directory.CreateDirectory(dir);
                File.WriteAllBytes(dir + "7za.exe", Resource1._7za);
            }
            return RUN(@".\files\7za.exe a -tzip " + path + " " + pathToFileOrFolder);
        }
        internal static string InstertAndroidManifest(string path, string path2decompilefolder)
        {
            return InsertToAPK(path, path2decompilefolder + @"\original\AndroidManifest.xml");
        }
        internal static string InstertMetaInfo(string path, string path2decompilefolder)
        {
            return InsertToAPK(path, path2decompilefolder + @"\original\META-INF");
        }

        internal static string Install(string path)
        {
            string dir = Environment.CurrentDirectory + @"\files\";
            if (!File.Exists(dir + "apktool.jar"))
            {
                Directory.CreateDirectory(dir);
                File.WriteAllBytes(dir + "apktool.jar", Resource1.apktool2);
            }
            return RUN(@"java -jar .\files\apktool.jar if " + path);
        }

        internal static string CompileAPK(string path, string path2newapk = "")
        {
            string dir = Environment.CurrentDirectory + @"\files\";
            if (!File.Exists(dir + "apktool.jar"))
            {
                Directory.CreateDirectory(dir);
                File.WriteAllBytes(dir + "apktool.jar", Resource1.apktool2);
            }
            if (path2newapk == "")
                return RUN(@"java -jar .\files\apktool.jar b " + path);
            else
                return RUN(@"java -jar .\files\apktool.jar b " + path + " -o " + path2newapk);
        }


        internal static string DecompileAPK(string path, string path2newdir = "")
        {
            string dir = Environment.CurrentDirectory + @"\files\";
            if (!File.Exists(dir + "apktool.jar"))
            {
                Directory.CreateDirectory(dir);
                File.WriteAllBytes(dir + "apktool.jar", Resource1.apktool2);
            }
            if (path2newdir == "")
                return RUN(@"java -jar .\files\apktool.jar d " + path);
            else
                return RUN(@"java -jar .\files\apktool.jar d " + path + " -o " + path2newdir);
        }
        internal static string SignAPK(string path)
        {
            string dir = Environment.CurrentDirectory + @"\files\";
            if (!File.Exists(dir + "signapk.jar"))
            {
                Directory.CreateDirectory(dir);
                File.WriteAllBytes(dir + "signapk.jar", Resource1.signapk);
            }
            if (!File.Exists(dir + "testkey.pk8"))
            {
                Directory.CreateDirectory(dir);
                File.WriteAllBytes(dir + "testkey.pk8", Resource1.testkey);
            }
            if (!File.Exists(dir + "testkey.x509.pem"))
            {
                Directory.CreateDirectory(dir);
                File.WriteAllBytes(dir + "testkey.x509.pem", Resource1.testkey_x509);
            }
            string result = RUN(@"java -jar .\files\signapk.jar .\files\testkey.x509.pem .\files\testkey.pk8 " + path + @" " + path + ".sign");
            File.Copy(path + ".sign", path, true);
            File.Delete(path + ".sign");
            return result;
        }


    }
}
