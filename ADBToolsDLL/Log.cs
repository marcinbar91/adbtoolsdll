﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ADBToolsDLL
{
    public class Log
    {
        static StringBuilder log = new StringBuilder();
        private static object m_SyncObject = new object();
        private static bool clear = false;



        public static void Append(string toadd)
        {
            lock (m_SyncObject)
            {
                log.Append(toadd);
            }
        }
        public static void AppendNewLine(string toadd)
        {
            lock (m_SyncObject)
            {
                if (log.Length != 0)
                    log.AppendLine();
                log.Append(toadd);
            }
        }

        public static string Get()
        {
            lock (m_SyncObject)
            {
                if (ClearIfGet)
                {
                    StringBuilder log2 = new StringBuilder();
                    log2 = log;
                    log.Clear();
                    return log2.ToString();
                }
                return log.ToString();
            }
        }

        public static void Clear()
        {
            log.Clear();
        }

        public static bool ClearIfGet
        {
            get { return clear; }

            set { clear = value; }
        }
    }
}
